from .context import pyatran  # noqa

from pyatran.runtime.runtime import rotate_log


def test_rotate_log_single(tmpdir):
    # there's one errors.log file, no older versions
    p = tmpdir.join("errors.log")
    p.write('')
    rotate_log(tmpdir)
    assert len(tmpdir.listdir()) == 1
    assert 'errors.log.0' in [fn.basename for fn in tmpdir.listdir()]
    tmpdir.remove()


def test_rotate_log_none(tmpdir):
    # there's no errors.log file
    rotate_log(tmpdir)
    assert len(tmpdir.listdir()) == 0
    tmpdir.remove()


def test_rotate_log_one(tmpdir):
    # there's one errors.log file and one errors.log.0
    for ii, fn in enumerate(['errors.log', 'errors.log.0']):
        p = tmpdir.join(fn)
        p.write(ii)
    rotate_log(tmpdir)
    assert len(tmpdir.listdir()) == 2
    assert 'errors.log.0' in [fn.basename for fn in tmpdir.listdir()]
    assert 'errors.log.1' in [fn.basename for fn in tmpdir.listdir()]
    for ii, fn in enumerate(['errors.log.0', 'errors.log.1']):
        p = tmpdir.join(fn)
        assert p.read() == str(ii)
    tmpdir.remove()


def test_rotate_log_one_with_number_gap(tmpdir):
    # there's one errors.log file and one errors.log.0
    for ii, fn in enumerate(['errors.log', 'errors.log.1']):
        p = tmpdir.join(fn)
        p.write(ii)
    rotate_log(tmpdir)
    assert len(tmpdir.listdir()) == 2
    assert 'errors.log.0' in [fn.basename for fn in tmpdir.listdir()]
    assert 'errors.log.2' in [fn.basename for fn in tmpdir.listdir()]
    for ii, fn in enumerate(['errors.log.0', 'errors.log.2']):
        p = tmpdir.join(fn)
        assert p.read() == str(ii)
    tmpdir.remove()


def test_rotate_log_filename_single(tmpdir):
    # there's one errortest.log file, no older versions
    p = tmpdir.join('errortest.log')
    p.write('')
    rotate_log(tmpdir, 'errortest.log')
    assert len(tmpdir.listdir()) == 1
    assert 'errortest.log.0' in [fn.basename for fn in tmpdir.listdir()]
    tmpdir.remove()


def test_rotate_log_filename_dot_single(tmpdir):
    # there's one error.test.log file, no older versions
    p = tmpdir.join('error.test.log')
    p.write('')
    rotate_log(tmpdir, 'error.test.log')
    assert len(tmpdir.listdir()) == 1
    assert 'error.test.log.0' in [fn.basename for fn in tmpdir.listdir()]
    tmpdir.remove()


def test_rotate_log_others_pressent(tmpdir):
    # there's one errors.log, and one errortest.log file, no older versions
    for ii, fn in enumerate(['errors.log', 'errortest.log', 'errors.log1']):
        p = tmpdir.join(fn)
        p.write(ii)
    rotate_log(tmpdir)
    assert len(tmpdir.listdir()) == 3
    for ii, fn in enumerate(['errors.log.0', 'errortest.log', 'errors.log1']):
        p = tmpdir.join(fn)
        assert p.read() == str(ii)
    tmpdir.remove()
