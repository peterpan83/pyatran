# -*- coding: utf-8 -*-

"""pyatran.params
=================

Creation of SCIATRAN control files from JSON.

.. autosummary::
   :toctree: api/

   json_to_sciatran
   load_json

"""

from .params import json_to_sciatran, load_json
